<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class FrontendController extends Controller
{
    public function index()
    {
        // $pertanyaan = DB::table('pertanyaan')->get();

        $posts = Post::all();

        return view('frontend.index', compact('posts'));
    }

    public function show($id)
    {
        $post = Post::find($id);

        return view('frontend.show', compact('post'));
    }
}

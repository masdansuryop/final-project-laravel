<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## TravelBlog
Merupakan website yang menampilkan artikel seputar traveling

### Kelompok 15
Nurdeka Hidayanto (@nurdeka)<br>
Masdan Suryo Purmadianto (@masdansuryop)<br>
Fahri ramadhan (@Fahri_ram/Fahri Ramadhan)

## Template
Backend : AdminLTE (https://adminlte.io/)<br>
Frontend : DirEngine (https://technext.github.io/direngine/index.html)

## Demo
Video Demo : https://drive.google.com/file/d/1ZfYApD9Q0MbhuntAOPltPy1BShDy0fkz/view?usp=sharing

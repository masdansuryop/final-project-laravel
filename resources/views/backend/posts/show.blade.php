@extends('backend.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Posts</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">TravelBlog</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('posts.index') }}">Posts</a></li>
                    <li class="breadcrumb-item active">Show</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size: 24px">Show</h3>
            <div class="card-tools">
                <a type="button" class="btn btn-block btn-outline-primary btn-sm" href="{{ route('posts.index') }}">
                    <i class="fas fa-arrow-left"></i>
                    &nbsp;Back
                </a>
            </div>
        </div>

        <div class="card-body">
            <img src='{{ asset("images/$post->picture") }}'' class="img-fluid" alt="Responsive image"
                style="width: 100%; height: 300px; object-fit: cover">
            <div class="card-body">
                <h3>{{ $post->title }}</h3>
                <p class="card-text"><i class="fas fa-user-alt"></i> John Doe </p>
                <p class="card-text">{{ $post->contents }}</p>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection
@extends('backend.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Posts</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">TravelBlog</a></li>
                    <li class="breadcrumb-item"><a href="#">Posts</a></li>
                    <li class="breadcrumb-item active">List</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size: 24px">List</h3>
            <div class="card-tools">
                <a type="button" class="btn btn-block btn-outline-primary btn-sm" href="{{ route('posts.create') }}">
                    <i class="fas fa-plus"></i>
                    &nbsp;Add Data
                </a>
            </div>
        </div>

        <div class="card-body">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                {{ session('success') }}
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        {{-- <th>Picture</th> --}}
                        <th>Title</th>
                        <th style="width: 40px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($posts as $key => $post)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        {{-- <td>{{ $post->picture }}</td> --}}
                        <td>{{ $post->title }}</td>
                        <td style="display: flex">
                            <a type="button" class="btn btn-outline-success btn-sm"
                                href="{{ route('posts.show', ['post' => $post->id]) }}"><i
                                    class="far fa-eye"></i></a>&nbsp;
                            <a type="button" class="btn btn-outline-warning btn-sm"
                                href="/posts/{{ $post->id }}/edit"><i class="far fa-edit"></i></a>&nbsp;
                            <form action="/posts/{{ $post->id }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-outline-danger btn-sm"><i
                                        class="far fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center"> No data </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection
@extends('backend.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Posts</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">TravelBlog</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('posts.index') }}">Posts</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size: 24px">Edit</h3>
            <div class="card-tools">
                <a type="button" class="btn btn-block btn-outline-primary btn-sm" href="{{ route('posts.index') }}">
                    <i class="fas fa-arrow-left"></i>
                    &nbsp;Back
                </a>
            </div>
        </div>

        <div class="card-body">
            <form role="form" action="/posts/{{ $post->id }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter title"
                            value="{{ old('title', $post->title) }}">
                    </div>
                    @error('title')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="picture">Picture</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="picture" name="picture"
                                    value="{{ old('contents', $post->picture) }}">
                                <label class="custom-file-label" for="picture">Choose file</label>
                            </div>
                        </div>
                    </div>
                    @error('picture')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="contents">Contents</label>
                        <textarea class="form-control" rows="5" id="contents" name="contents"
                            placeholder="Enter contents">{{ old('contents', $post->contents) }}</textarea>
                    </div>
                    @error('contents')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection
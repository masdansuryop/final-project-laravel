<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
  </ul>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <!-- Profile Dropdown Menu -->
    <li class="dropdown user user-menu">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src='{{ asset("images/".Auth::user()->profile->avatar) }}' class="user-image" alt="User Image"
          style="width: 33.59px; height: 33.59px; object-fit:cover">
        <span class="hidden-xs">{{ Auth::user()->profile->fullname }}</span>
      </a>
      <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
          <img src="{{ asset("images/".Auth::user()->profile->avatar) }}" class="img-circle" alt="User Image"
            style="width: 90px; height: 90px; object-fit:cover">

          <p>
            {{ Auth::user()->profile->fullname }}
            <small>Member since {{ Auth::user()->profile->created_at }}</small>
          </p>
        </li>
        <!-- Menu Footer-->
        <li class="user-footer" style="text-align: center">
          <a href="/profiles/{{ Auth::user()->id }}/edit" class="btn btn-default btn-flat">Profile</a>
          <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
            {{ __('Logout') }}</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </li>
      </ul>
    </li>
  </ul>
</nav>
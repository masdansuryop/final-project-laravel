{{-- <a href="#" class="d-block">{{ Auth::user()->name }}</a> --}}

<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="{{ asset('backend/index3.html') }}" class="brand-link">
    <img src="{{ asset('backend/dist/img/AdminLTELogo.png') }}" alt="TravelBlog Logo"
      class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">TravelBlog</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset("images/".Auth::user()->profile->avatar) }}" class="img-circle elevation-2" alt="User Image"
          style="width: 33.59px; height: 33.59px; object-fit:cover">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{ Auth::user()->profile->fullname }}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="/" class="nav-link">
            <i class="nav-icon fab fa-blogger"></i>
            <p>
              Blogs
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('posts.index') }}" class="nav-link">
            <i class="nav-icon far fa-file-alt"></i>
            <p>
              Posts
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('comments.index') }}" class="nav-link">
            <i class="nav-icon far fa-comments"></i>
            <p>
              Comments
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{ route('categories.index') }}" class="nav-link">
            <i class="nav-icon fab fa-buffer"></i>
            <p>
              Categories
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
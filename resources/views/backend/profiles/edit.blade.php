@extends('backend.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Profiles</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">TravelBlog</a></li>
                    <li class="breadcrumb-item"><a href="#">Profiles</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size: 24px">Edit</h3>
        </div>

        <div class="card-body">
            @if (session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Alert!</h4>
                {{ session('success') }}
            </div>
            @endif
            <form role="form" action="/profiles/{{ $profile->id }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="avatar">Avatar</label><br>
                        @if ($profile->avatar)
                        <img src='{{ asset("images/$profile->avatar") }}' class="img-circle" alt="Avatar"
                            id="avatar_preview" style="width: 100px; height: 100px; object-fit: cover"><br><br>
                        @endif
                        <input type="file" id="avatar" name="avatar" value="{{ old('avatar', $profile->avatar) }}"
                            class="@error('avatar') is-invalid @enderror">
                        @error('avatar')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror

                        <p class="help-block">Format (jpeg|png|jpg|gif|svg) Max (2 Mb)</p>
                    </div>
                    <div class="form-group">
                        <label for="fullname">Fullname</label>
                        <input type="text" class="form-control @error('fullname') is-invalid @enderror" id="fullname"
                            name="fullname" placeholder="Enter fullname"
                            value="{{ old('fullname', $profile->fullname) }}">
                        @error('fullname')
                        <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection

@push('script')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#avatar_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar").change(function(){
        readURL(this);
    });
</script>
@endpush
@extends('backend.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Categories</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">TravelBlog</a></li>
                    <li class="breadcrumb-item"><a href="#">Categories</a></li>
                    <li class="breadcrumb-item active">List</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="font-size: 24px">List</h3>
            <div class="card-tools">
                <a type="button" class="btn btn-block btn-outline-primary btn-sm"
                    href="{{ route('categories.create') }}">
                    <i class="fas fa-plus"></i>
                    &nbsp;Add Data
                </a>
            </div>
        </div>

        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Task</th>
                        <th>Progress</th>
                        <th style="width: 40px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td>Update software</td>
                        <td></td>
                        <td style="display: flex">
                            <a type="button" class="btn btn-outline-success btn-sm" href="#"><i
                                    class="far fa-eye"></i></a>&nbsp;
                            <a type="button" class="btn btn-outline-warning btn-sm" href="#"><i
                                    class="far fa-edit"></i></a>&nbsp;
                            <a type="button" class="btn btn-outline-danger btn-sm" href="#"><i
                                    class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
<!-- /.content -->
@endsection